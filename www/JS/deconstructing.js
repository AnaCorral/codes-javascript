// NUEVA FORMA DE COGER VALORES PARA NUEVO ARRAY (DESTRUCTURING)

let names = ['Ivan', 'Roberto', 'Pablo', 'Ana', 'Andrea'];

let [ivan, roberto, ...rest] = names;

console.log(ivan, roberto, rest);

// REST mete en un array los que faltan por procesar

// Meter nombre y apellidos en un Array y si falta que no te dé undefined

let name = ['Ana', 'Corral'];

let [nombre, apellido1, apellido2 = 'noData'] = name;

console.log(nombre, apellido1, apellido2);

// CON OBJETOS Y GUARDAR EL RESTO

let teacher = {
  name2: 'Ana',
  city: 'Coruña',
  age: 24,
  pet: 'dog'
};

let { name2 = noName, city, ...rest2 } = teacher; //Dá igual el orden, los coje por orden

console.log(name2, city, rest2);
