// Crea un elemento con un valor

const myMap = new Map();
const myObject = {};
const myFunction = () => {};

// Almacenar datos

myMap.set('un string', 'El valor de un string');
myMap.set(myObject, 'El valor de un onbjeto');
myMap.set(myFunction, 'El valor de una funcion');

console.log(myMap);
console.log(myMap.size);

//recuperar valores

console.log(myMap.get('un string'));
console.log(myMap.get(myObject));
console.log(myMap.get(myFunction));

for (const item of myMap.keys()) {
  // para cojer los elementos(izq) de cada Array
  console.log('map key ->', item);
}

for (const item of myMap.values()) {
  // para cojer los valores(drch) de cada Array
  console.log('map key ->', item);
}

for (const item of myMap) {
  // para cojer los elementos y valores de cada Array
  console.log('map key ->', item);
}

// Recorre todos los elementos también

myMap.forEach((value, key) => {
  // Valor y despues la clave para ese valor
  console.log('forEach ->', key, value);
});

// Copia poco profunda, no son iguales, son instancias nuevas. Si se referencia a algo en común, la referencia la mantiene pero cambia los valores si se modifica en alguno de los dos.

const clone = new Map(myMap);

console.log(clone);

// Other map

const otherMap = new Map();
otherMap.set(1, 14);

console.log(otherMap);

// Mapa combinacion de los dos

const mergeMap = new Map([...myMap, ...otherMap]);
console.log(mergeMap);

// SETS Como Array, no tiene ducplicado, valores duplicados

const mySet = new Set([1, 1, 1, 1, 1, 1, 1, 1, 2, 3, 4, 5]);
const myOtherSet = new Set([9, 8, 7]);

mySet.add('algo');
mySet.add('algo');
console.log(mySet);

// Borrar el 3

mySet.delete(3);
console.log(mySet);

// const numbers = [2, 4, 5, 7, 9, 9, 9, 9, 7, 7, 7, 5, 4, 4, 2, 4, 7, 0];
// const unique = [...new Set(numbers)];
// console.log(unique);

// Saber si tiene algún elemento

console.log(mySet.has(4));
console.log(mySet.has({}));

for (const item of mySet.keys()) {
  // para cojer los elementos(izq) de cada Array
  console.log('set key ->', item);
}

mySet.forEach((value, key) => {
  // Valor y despues la clave para ese valor
  console.log('forEach ->', key, value);
});

//Crear Array de Set (2 formas)

console.log(Array.from(mySet));
console.log([...mySet]);

// Combinar dos sets

const combined = new Set([...mySet, ...myOtherSet]);
console.log(combined);