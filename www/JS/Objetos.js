let numero = [7, 5, 10, 3, 4];
console.log(numero);

function ordenar(num) {
  let nuevoNumero = [...numero];

  // ordena menor a mayor
  //   for (let i = 0; i < nuevoNumero.length; i++) {
  //     numero.sort((numero1, numero2) => numero1 - numero2);
  //     return nuevoNumero;
  //     console.log(nuevoNumero);
  //   }

  // ordena mayor a menor
  for (let i = 0; i < nuevoNumero.length; i++) {
    numero.sort((numero1, numero2) => numero2 - numero1);
    return nuevoNumero;
  }
}

ordenar(numero);
console.log(ordenar(numero));

// Profesor

// const desordenado = [1, 4, 6, 2];

// let ordenado = [...desordenado].sort((a, b) => a - b);
// console.log(ordenado);

// Otra manera

let numero2 = [3, 20, 8, 5, 3];
console.log(numero2);

function ordenar2(nums) {
  let nuevoNumero2 = [...numero2];

  for (let i = 0; i < nums.length; ) {
    if (nuevoNumero2[i] > nuevoNumero2[i + 1]) {
      nuevoNumero2.push(nuevoNumero2[i]);
      nuevoNumero2.splice(i, 1);
      i = 0;
    } else {
      i++;
    }
  }
  console.log(nuevoNumero2);
}

ordenar2(numero2);

// PROFESOR

const desordenado2 = [3, 8, 20, 9];

function ordenarArray(elDato) {
  const datoOrdenado = [...elDato];

  for (let i = 0; i < datoOrdenado.length; i++) {
    for (let j = 0; j < datoOrdenado.length; j++) {
      if (datoOrdenado[i] < datoOrdenado[j]) {
        const temp = datoOrdenado[j];
        datoOrdenado[j] = datoOrdenado[i];
        datoOrdenado[i] = temp;
      }
    }
  }
  return datoOrdenado;
}

const ordenado = ordenarArray(desordenado2);
console.log(ordenado);

//CALLBACK

const nuestroCallback = function(texto) {
  console.log(texto);
};

//Hoisting
function otroCallback(texto) {
  console.log(texto[1]);
}

function eco(unCallback) {
  const contenido = 'hola';
  console.log(contenido);
  unCallback(contenido);
}

eco(nuestroCallback);
eco(otroCallback);

// CALLBACK + FOREACH

// const dato = [3, 7, 10, 8];

// function foreach(datos) {
//   for (let i = 0; i < dato.length; i++) {
//     if (datos[i] === 10) {
//       return true;
//     }
//   }
//   return false;
// }

// function tiene10foreach(datos) {
//   datos.forEach((elemento) => {
//     if (elemento === 10) {
//       return true;
//     }
//   });
//   return false;
// }

// console.log(foreach(dato));
// console.log(tiene10foreach(dato));

const dato = [3, 7, 10, 8];

function incrementa(dato, cantidad) {
  return dato.map((elemento) => {
    return elemento + cantidad;
  });
}

const incrementado = incrementa(dato, 5);
console.log(incrementado);

const data = [1, 10, 3, 14, 5];

const filteredData = data.filter((item) => item >= 10);

console.log(filteredData);

const data2 = [1, 10, 3, 14, 5];

const reduceData = data2.reduce((accumulator, currentValue) => {
  return accumulator * currentValue;
}, 5);

// Valor inicial del accumulator

console.log(reduceData);

const data3 = [1, 10, 3, 14, 5];

for (const item of data) {
  console.log(item);
}

// JUNTAR NOMBRES CON EDADES

function miNuevoArray() {
  let nombresPersonas = ['Ana', 'Iago'];
  let edadPersonas = [24, 33];
  let arrayUsuarios = [];

  for (let i = 0; i < nombresPersonas.length; i++) {
    arrayUsuarios.push({
      nombre: nombresPersonas[i],
      edad: edadPersonas[i]
    });
  }
  console.log(arrayUsuarios);
}

miNuevoArray();

// Con nuevas herramientas

// const userNames = ['Ana', 'Iago', 'Carlos'];
// const userAges = [16, 33, 39];

// function createUsers(names, ages) {
//   return names.map((userName, index) => {
//     const users = { name: userName, age: ages[index] };
//     console.log(users);
//   });
// }

// function createUsersOfAge(names, ages) {
//   return ages.map((userAges, index) => {
//     if (userAges > 18) {
//       return { name: names[index], age: userAges, adult: true };
//     } else if (userAges < 18) {
//       return { name: names[index], age: userAges, adult: false };
//     }
//   });
// }

// createUsers(userNames, userAges);

// const userAge = createUsersOfAge(userNames, userAges);
// console.log(userAge);

console.log('JUNTAR NOMBRE Y EDAD USUARIOS Y QUE SOLO APAREZCAN LOS MAYORES DE EDAD');

const names = ['Ivan', 'Lucia', 'Ana'];
const ages = [23, 14, 78];

const users = names.map((nameValue, index) => {
  return { name: nameValue, age: ages[index] };
});

const adultUsers = users.map((user) => {
  const temp = { ...user }; // HACE DEEP COPY DEL OBJETO
  temp.isAdult = temp.age >= 18 ? true : false;
  return temp;
});

const filteredAdults = adultUsers.filter((user) => {
  return user.isAdult;
});

console.log(filteredAdults);

const totalAge = filteredAdults.reduce((previusUser, currentUser) => {
  return previusUser + currentUser.age; // return previusUser + 1;
}, 0);

console.log(totalAge);

// TODO EN UNA MISMA CONSTANTE

// const names = ['Ivan', 'Lucia', 'Ana'];
// const ages = [23, 14, 78];

// const users = names
//   .map((nameValue, index) => {
//     return { name: nameValue, age: ages[index] };
//   })
//   .map((user) => {
//     const temp = { ...user }; // HACE DEEP COPY DEL OBJETO
//     temp.isAdult = temp.age >= 18 ? true : false;
//     return temp;
//   })
//   .filter((user) => {
//     return user.isAdult;
//   })
//   .reduce((previusUser, currentUser) => {
//     return previusUser + currentUser.age;
// return previusUser + 1; // Cuantos mayores de edad hay
//   }, 0);

// console.log(users);

console.log('FUNCION RECURSIVA');

function factorial(data) {
  if (data === 0) {
    return 1;
  }
  return data * factorial(data - 1);
}
console.log(factorial(6));

// FACTORIAL POR BUCLE
console.log('----------Factorial por BUCLE----------');

function FactorialBucle(numeroFactorial) {
  let result = 1;
  for (let i = 1; i <= numeroFactorial; i++) {
    result = result * i;
  }
  return result;
}

console.log(FactorialBucle(6));

// FIBONACCI   Lo considera como un Array
console.log('----------Fibonacci----------');

function fibonacci(amount) {
  if (amount === 0) {
    return 0;
  }
  if (amount === 1) {
    return 1;
  }
  return fibonacci(amount - 1) + fibonacci(amount - 2);
}
console.log(fibonacci(6));

// CONSTRUIR OBJETOS
console.log('CONSTRUIR OBJETO');

function Dog(name) {
  this.name = name;
  this.speak = function() {
    console.log(this.name);
  };
}

const myDog = new Dog('Vega');
const otherDog = new Dog('Mara');

console.log(myDog);
console.log(otherDog);

myDog.speak();
otherDog.speak();

// CON CLASS => no tiene Hoisting

class Cat {
  constructor(name) {
    this.name = name;
  }

  speak(wat) {
    return wat + ' ' + this.name;
  }
}

const myCat = new Cat('Bola');
console.log(myCat);
console.log(myCat.speak('miau'));

// Ver si los usuarios son mayor de edad

class User {
  #age;
  name;
  address = 'sin direccion';
  constructor(name, age) {
    this.name = name;
    this.#age = age;
  }

  isAdult() {
    // En cada una de las instancias
    return this.age >= 18;
  }

  static babyAge() {
    //De la misma clase
    return 3;
  }

  addAddress(address) {
    this.address = address;
  }
}

const me = new User('Ana', 24);
console.log(me.isAdult());
console.log(me.name);
console.log(me.age);
me.addAddress('aqui');
console.log(me.address);
console.log(User.babyAge());

// HERENCIAS

class Animal {
  constructor(name) {
    this.name = name;
  }
}

class Duck extends Animal {
  constructor(name) {
    super(name); // LLama al constructor de arriba o el método
    this.name += ' the duck';
  }

  speak() {
    console.log('CUAK');
  }
}

class Bear extends Animal {
  constructor(name) {
    super(name); // LLama al constructor de arriba o el método
    this.name += ' the bear';
  }

  speak() {
    console.log('GRR');
  }
}

const myAnimal = new Animal('Un animal');
const myDuck = new Duck('Lola');
const myBear = new Bear('Oso');

console.log(myAnimal, myDuck, myBear);

myDuck.speak();
myBear.speak();