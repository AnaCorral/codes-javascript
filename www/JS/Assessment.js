"use strict";

// // CALCULADORA
// // ESTÁ COMENTADO YA QUE SE INICIALIZA CON PROMPT

// function program() {
//   let emergent = prompt(
//     "¿Qué operación quieres hacer? Elige entre restar, sumar, multiplicar, dividir o exponenciar."
//   );
//   console.log(emergent);

//   let numero1 = prompt("Indica número 1");
//   console.log(numero1);
//   let numero2 = prompt("Indica número 2");
//   console.log(numero2);

//   if (emergent === "sumar") {
//     console.log(alert(parseInt(numero1) + parseInt(numero2)));
//   }

//   if (emergent === "restar") {
//     console.log(alert(parseInt(numero1) - parseInt(numero2)));
//   }

//   if (emergent === "dividir") {
//     console.log(alert(parseInt(numero1) / parseInt(numero2)));
//   }

//   if (emergent === "exponenciar") {
//     console.log(alert(Math.pow(parseInt(numero1), parseInt(numero2))));
//   }
// }

// console.log(program());

// // REGISTRO ACADÉMICO

const studentsNames = ["Ana", "Carlos", "Martin", "Maria", "Iago", "Cristina"];
const studentsAge = [20, 20, 19, 20, 20, 19];
const studentsGender = [
  "Mujer",
  "Hombre",
  "Hombre",
  "Mujer",
  "Hombre",
  "Mujer"
];

class Person {
  constructor(name, age, gender) {
    this.name = name;
    this.age = age;
    this.gender = gender;
  }

  static showProperties(name, age, gender) {
    return name.map((name, index) => {
      return new Person(name, age[index], gender[index]);
    });
  }
}

const createdStudents = Person.showProperties(
  studentsNames,
  studentsAge,
  studentsGender
);
console.log(createdStudents);

// ALUMNOS CON NOMBRE DE LA A A LA L -> GRUPO A
// ALUMNOS CON 19 AÑOS -> CURSO 1º

createdStudents[0].grupo = "Grupo A";
createdStudents[0].curso = "2º";
createdStudents[1].grupo = "Grupo A";
createdStudents[1].curso = "2º";
createdStudents[2].grupo = "Grupo B";
createdStudents[2].curso = "1º";
createdStudents[3].grupo = "Grupo B";
createdStudents[3].curso = "2º";
createdStudents[4].grupo = "Grupo A";
createdStudents[4].curso = "2º";
createdStudents[5].grupo = "Grupo A";
createdStudents[5].curso = "1º";

console.log(createdStudents);

class Students extends Person {
  constructor(name, age, gender, course, group) {
    super(name, age, gender);
    this.course = course;
    this.group = group;
  }
}

const studentsCurso1 = [];
const studentsCurso2 = [];

function grupos() {
  for (let i = 0; i < createdStudents.length; i++) {
    if (createdStudents[i].curso === "1º") {
      studentsCurso1.push(createdStudents[i]);
    }
    if (createdStudents[i].curso === "2º") {
      studentsCurso2.push(createdStudents[i]);
    }
  }
}

grupos();

console.log(studentsCurso1);
console.log(studentsCurso2);

const teacherName = ["Lucas", "Mario", "Merche"];
const teacherAge = [33, 42, 30];
const teacherGender = ["Hombre", "Hombre", "Mujer"];
const teacherSubject = ["Educación Física", "Inglés", "Español"];
const teacherGroup = ["1º", "2º", "Grupo A"];

class Teacher extends Person {
  constructor(name, age, gender, subject, group) {
    super(name, age, gender);
    this.subject = subject;
    this.group = group;
  }

  static showProperties2(name, age, gender, subject, group) {
    return name.map((name, index) => {
      return new Teacher(
        name,
        age[index],
        gender[index],
        subject[index],
        group[index]
      );
    });
  }
}

const createdTeachers = Teacher.showProperties2(
  teacherName,
  teacherAge,
  teacherGender,
  teacherSubject,
  teacherGroup
);
console.log(createdTeachers);

createdTeachers[0].studentsList = [];
createdTeachers[1].studentsList = [];
createdTeachers[2].studentsList = [];

function studentsList() {
  for (let i = 0; i < createdStudents.length; i++) {
    if (createdStudents[i].curso === "1º") {
      createdTeachers[0].studentsList.push(createdStudents[i]);
    }

    if (createdStudents[i].curso === "2º") {
      createdTeachers[1].studentsList.push(createdStudents[i]);
    }

    if (createdStudents[i].grupo === "Grupo A") {
      createdTeachers[2].studentsList.push(createdStudents[i]);
    }
  }
}

studentsList();
console.log(createdTeachers);

console.log("--------------------------------");
console.log("·PROFESOR  |  ", createdTeachers[0].name);
console.log("Edad       |  ", createdTeachers[0].age);
console.log("Género     |  ", createdTeachers[0].gender);
console.log("Asignatura         |  ", createdTeachers[0].subject);
console.log("Grupo a dar clase  |  ", createdTeachers[0].group);
console.log("Alumnos            |  ", createdTeachers[0].studentsList);
console.log("--------------------------------");
console.log("·PROFESOR  |  ", createdTeachers[1].name);
console.log("Edad       |  ", createdTeachers[1].age);
console.log("Género     |  ", createdTeachers[1].gender);
console.log("Asignatura         |  ", createdTeachers[1].subject);
console.log("Grupo a dar clase  |  ", createdTeachers[1].group);
console.log("Alumnos            |  ", createdTeachers[1].studentsList);
console.log("--------------------------------");
console.log("·PROFESOR  |  ", createdTeachers[2].name);
console.log("Edad       |  ", createdTeachers[2].age);
console.log("Género     |  ", createdTeachers[2].gender);
console.log("Asignatura         |  ", createdTeachers[2].subject);
console.log("Grupo a dar clase  |  ", createdTeachers[2].group);
console.log("Alumnos            |  ", createdTeachers[2].studentsList);

// DADO ELECTRÓNICO

function dado() {
  let gameover = 50;
  let numeroInicial = Math.round(Math.random() * 6);
  console.log(numeroInicial);

  while (numeroInicial < gameover) {
    let numbers = [];
    let numero = Math.round(Math.random() * 6);
    console.log("+ ", numero);
    numeroInicial = numeroInicial + numero;
    numbers.push(numeroInicial);
    console.log(numbers);
  }
  console.log("FIN DEL JUEGO");
}

dado();