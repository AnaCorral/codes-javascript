// Pedir al servidor

// let allBreeds;

// fetch('https://dog.ceo/api/breeds/list/all')
//   .then((fetchData) => {
//     return fetchData.json();
//   })
//   .then((breeds) => {
//     allBreeds = breeds;
//     console.log(allBreeds);
//   });

// console.log(allBreeds); // EJECUTA PRIMERO, pero si lo pones en la consola tenemos todos los datos

// NUEVO FOR

// let myCharacter;

// fetch('https://rickandmortyapi.com/api/character/2').then((fetchData) => {
//   return fetchData.json().then((character) => {
//     myCharacter = character;
//     for (const data in myCharacter) {
//       if (myCharacter.hasOwnProperty(data)) {
//         console.log('Atributo', data, 'Dato', myCharacter[data]);
//       }
//     }
//     console.log(myCharacter, name);
//     console.log('toString' in myCharacter); // True heredadas o no
//     console.log(myCharacter.hasOwnProperty('toString')); // True si es heredada solo
//   });
// });

// COGER TEXTO NO EL OBJETO

let myCharacter;

fetch('https://rickandmortyapi.com/api/character/2').then((fetchData) => {
  return fetchData.json().then((character) => {
    console.log(JSON.stringify(character));
  });
});

const myData = '{"a": 14}';

console.log(JSON.parse(myData));

// COPIA PROFUNDA

// fetch('https://rickandmortyapi.com/api/character/2').then((fetchData) => {
//   return fetchData.json().then((character) => {
//     const stringify = JSON.stringify(character);
//     const newCopy = JSON.parse(stringified);
//   });
// });