// let aValue = 0;

// const theTimeout = setTimeout(() => {
//   aValue = 8;
//   console.log('cambio');
// }, 5000);

// setTimeout(() => {
//   console.log(aValue);
//   clearTimeout(theTimeout);
// }, 500);

// const theInterval = setInterval(() => {
//   console.log('El valor es ', aValue);
// }, 500);

// setTimeout(() => {
//   console.log(aValue);
//   clearInterval(theInterval);
// }, 6000);

// // TRY CATCH

// try {
//   aFunction();
// } catch (error) {
//   console.log('NO VA', error);
// } finally {
//   console.log('HEY');
// }

// try {
//   throw 'myException';
// } catch (error) {
//   console.log('NO VA', error);
// } finally {
//   console.log('HEY');
// }

/////// PROMESAS

// const age = 19;

// const myPromise = new Promise((resolve, reject) => {
//   // Clase reservada
//   // setTimeout(() => {
//   //   resolve('Ahora tengo el valor'); // Te lo voy a dar, pero despues
//   // }, 2000);

//   if (age < 18) {
//     resolve('menor de edad');
//   } else {
//     reject('mayor de edad');
//   }
// });

// // Necesita función de Callback con resolve (todo va bien), reject
// myPromise
//   .then((theValue) => {
//     //Como parámetro el valor con el que hemos resuelto, return de la promesa
//     console.log(theValue);
//   })

//   .catch((theValue) => {
//     console.log(theValue);
//   })

//   .finally(() => {
//     console.log('Esto al final');
//   });

// ALL , Cuando se ejecutan todas (ARRAY)

function promiseGenerator() {
  const randomNumber = Math.random() * 10000; // Se resuelve entre 0 y 10 segundos
  return new Promise((resolve) => {
    console.log('Espero ->', randomNumber);
    setTimeout(() => {
      console.log('Resolviendo ->', randomNumber);
      resolve(randomNumber);
    }, randomNumber);
  });
}

// const allPromises = [];
// allPromises.push(promiseGenerator());
// allPromises.push(promiseGenerator());
// allPromises.push(promiseGenerator());

// // Promise.all(allPromises).then((allData) => {
// //   console.log(allData);
// // });

// let test;

// Promise.race(allPromises)
//   .then((allData) => {
//     console.log('RACE!', allData);
//     test = 'Hola ' + allData;
//   })
//   .then((data) => {    // Permite operar con el dato fuera del Callback
//     console.log(test);
//   });

// ASYNC

async function oneFunction() {
  //Devuelve promesa
  const aValue1 = await promiseGenerator();
  console.log('WAIT1', aValue1);
  const aValue2 = await promiseGenerator();
  console.log('WAIT2', aValue2);
  const aValue3 = await promiseGenerator();
  console.log('WAIT3', aValue3);
}

oneFunction();