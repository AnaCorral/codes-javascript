
// habitacion (zorro, gallina)
// zorro comer y dice algo
// si hay 2 iguales se crea uno nuevo
// si son ditintos el zorro se come a la gallina

class Animal {} // CONSTRUCTOR IMPLÍCITO

class Chicken {
  breed() {
    return new Chicken(); // () EJECUTA EL CONSTRUCTOR
  }
}

class Fox {
  breed() {
    return new Fox();
  }
  eat(animals) {
    animals.filter(() => {
      return animals instanceof Fox;
    });
  }
}

class Room {
  putAnimals(animals) {
    this.animalsInside = animals;
  }

  closeDoor() {
    if (this.animalsInside[0].constructor.name === this.animalsInside[1].constructor.name) {
      this.animalsInside.push(this.animalsInside[0].breed());
    } else {
      for (let i = 0; i < this.animalsInside.length; i++) {
        if (this.animalsInside[i] instanceof Fox) {
          this.animalsInside = this.animalsInside[i].eat(this.animalsInside);
          break;
        }
      }
    }
  }

  takeAnimalsOut() {
    return this.animalsInside;
  }
}

const ourAnimals = []; // DOS ANIMALES ALEATORIOS
for (let i = 0; i < 2; i++) {
  const newAnimal = Math.random() * 2 > 1 ? new Fox('Arsenio') : new Chicken('Rodolfa');
  ourAnimals.push(newAnimal);
}
console.log(ourAnimals);

const ourRoom = new Room();

ourRoom.putAnimals(ourAnimals);
ourRoom.closeDoor();

const survivors = ourRoom.takeAnimalsOut();
console.log(survivors);

// Micro e-commerce
// Usuario, articulos (array con items) y tienda
// el usuario quiere coger items
// agrega a un carrito
// dice a la tienda (nuevoArray) y saca en consola ticket (cantidad unidades, nombre, precio y total
// al final total de los totales)

class Items {
  constructor(name, price) {
    this.name = name;
    this.price = price;
  }
}
class Users {
  constructor() {
    this.itemsBag = [];
  }
  addItems(item) {
    for (let i = 0; i < this.itemsBag.length; i++) {
      if (item.name === this.itemsBag[i].name) {
        this.itemsBag[i].units += 1;
        return;
      }
    }
    this.itemsBag.push({ ...item, units: 1 });
    return this.itemsBag;
  }
}

class Shop {
  seeBag(itemsBag, index) {
    let total = 0;
    console.log('TICKET');
    for (let i = 0; i < itemsBag.length; i++) {
      console.log(
        'Producto ' +
          itemsBag[i].name +
          ' Unidades ' +
          itemsBag[i].units +
          ' Precio ' +
          itemsBag[i].price +
          ' Total ' +
          itemsBag[i].price * itemsBag[i].units
      );
      total += itemsBag[i].price * itemsBag[i].units;
    }
    console.log('Total a pagar: ', total);
  }
}

const Shirt = new Items('Shirt', 10);
const Trousers = new Items('Trousers', 20);
const Shoes = new Items('Shoes', 30);
const myUser = new Users();
const myShop = new Shop();
myUser.addItems(Shirt);
myUser.addItems(Shirt);
myUser.addItems(Trousers);
myUser.addItems(Shoes);

console.log(myUser.itemsBag);
myShop.seeBag(myUser.itemsBag);