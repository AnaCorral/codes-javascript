// SOSPECHOSOS Y DETECTIVE

const name = ['Willy', 'Ivan', 'Ramiro'];
const eyeColor = ['azul', 'marron', 'azul'];
const height = ['bajo', 'alto', 'bajo'];
const tattoed = [true, false, false];
const tip = [{ height: 'alto' }, { eyeColor: 'marron' }, { tattoed: false }];

class Person {
  constructor(name) {
    this.name = name;
  }
}

class Suspect extends Person {
  #tip;
  // country = 'Spain' POR DEFECTO ESPAÑA
  constructor(name, eyeColor, height, tattoed, tip) {
    super(name);
    this.eyeColor = eyeColor;
    this.height = height;
    this.tattoed = tattoed;
    this.#tip = tip;
  }

  static createSuspects(name, eyeColor, height, tattoed, tip) {
    return name.map((name, index) => {
      return new Suspect(name, eyeColor[index], height[index], tattoed[index], tip[index]);
    });
  }

  confess() {
    return this.#tip;
  }
}

class Detective extends Person {
  constructor(name) {
    super(name);
  }

  investigate(suspects) {
    console.log('Estoy investigando ', suspects);
    let summary = {}; // Nos añada las pistas

    for (const suspect of suspects) {
      const theTip = suspect.confess(); // Llamamos al método confess
      summary = { ...summary, ...theTip }; // Copia summary y añade a copia de tip, para que no se sobreescriba
    }

    console.log(summary);

    const attributes = Object.keys(summary); //Creamos array con los atributos
    console.log(attributes);

    const convicts = suspects.filter((suspect) => {
      //filtramos array de sospechosos
      let guilty = true; // Por defecto es culpable
      for (const attribute of attributes) {
        // Recorremos con atributos de sumario y lo compara con array de sospechosos
        if (suspect[attribute] !== summary[attributes]) {
          guilty = false; // Si no es igual se vuelve false
          return guilty;
        }
      }
      return guilty;
    });

    return this.writeReport(convicts); // Llama a método del objeto
  }

  writeReport(convicts) {
    let report = 'The convict are: ';
    for (const convict of convicts) {
      report += ' ' + convict.name;
    }
    return report;
  }
}

const suspects = Suspect.createSuspects(name, eyeColor, height, tattoed, tip);
console.log(suspects);

const myDetective = new Detective('Colombo');

console.log(myDetective.investigate(suspects));