'use strict';

// Clase para numeros para una quiniela con unos metodos que devuleven o un numero (Cuando se le ocurra), y lo devuelve con una promesa. (con timeout)
// Tambien que devuelva todos los numeros que quiera (Con array) (Cada numero un tiempo)
// Metodo que de un numero y otro que de varios

class Gambler {
  static givePrediction() {   // No hay que instanciar la clase, se llama al método desde fuera ( no hace falta un new)
    const prediction = Math.ceil(Math.random() * 50);
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve(prediction);
      }, prediction * 100);
    });
  }

  static givePredictions(numPredictions) {
    const predictions = [];

    for (let i = 0; i < numPredictions; i++) {
      predictions.push(this.givePrediction());
    }
    return predictions;
  }
}

Gambler.givePrediction().then((myNumber) => {
  console.log(myNumber);
});

Promise.all(Gambler.givePredictions(10)).then((values) => {
  console.log(values);
});